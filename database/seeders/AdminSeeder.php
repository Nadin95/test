<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\admin;
use Illuminate\Support\Facades\Hash;


class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $username='admin';
        $password=Hash::make(123456);
        $mail='admin@gmail.com';


        $admin=admin::create([
            'username'=>$username,
            'password'=>$password,
            'mail'=>$mail
        ]);


    }
}
