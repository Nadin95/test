<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('location_id')->nullable();
            $table->foreign('location_id')->nullable()->references('id')->on('location')->onUpdate('cascade')->onDelete('cascade');

            $table->unsignedBigInteger('hotel_id')->nullable();
            $table->foreign('hotel_id')->nullable()->references('id')->on('hotel')->onUpdate('cascade')->onDelete('cascade');

            $table->string('url')->nullable();

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image');
    }
};
