var hotel={

    delete: function (id) {



        var chk=confirm('Are you sure ?');


        if(chk){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN':
                        $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/admin/hotel/deleteHotel",
                type: "POST",
                cache: false,
                data: {

           id:id
                },
                success: function (response) {
                    if(response=="success"){
                        console.log('data has been deleted');
                       window.location.reload();
                    }else{
                        console.log('Fail To delete');
                        window.location.reload();


                    }




                }, error: function (xhr) {
                    console.log('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                    alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);




                }
            })

        }
    },

    edit: function (id) {



        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN':
                    $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "/admin/hotel/edit",
            type: "POST",
            cache: false,
            data: {

           id:id
            },
            success: function (response) {

                if(response){
                    $('#hid').val(response.id);
                    $('#name').val(response.name);
                    $('#des').val(response.description);
                    $("#location_id").val(response.location_id);



                }else{
                    alert('Error')
                }



            }, error: function (xhr) {
                console.log('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);

                // $('#title_error').text(xhr.responseJSON.errors.grn_title);
                // $('#description_error').text(xhr.responseJSON.errors.grn_description );
                // $('#quertity_error').text(xhr.responseJSON.errors.quentity );
                // $('#supplier_error').text(xhr.responseJSON.errors.supplier);



            }
        })


}

}
