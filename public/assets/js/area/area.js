var area={
    save: function () {


        // alert('hi');

        var name=$('#name').val();
        var description=$('#des').val();
        var id=$('#hid').val();




            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN':
                        $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/admin/saveArea",
                type: "POST",
                cache: false,
                data: {

                            id:id,
                            name:name,
                            description:description,


                },
                success: function (response) {
                    if(response=="created"){
                        console.log('data has been saved');
                        alert('data has been saved')
                       window.location.reload();
                    }else if(response=="updated"){
                        console.log('data has been Updated');
                        alert('data has been updated');
                        window.location.reload();


                    }




                }, error: function (xhr) {
                    console.log('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                    alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);

                    // $('#name_error').text(xhr.responseJSON.errors.category_name);
                    // $('#dis_error').text(xhr.responseJSON.errors.description);
                    // $('#code_error').text(xhr.responseJSON.errors.code);




                }
            })


    },

    edit: function (id) {



            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN':
                        $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/admin/editArea",
                type: "POST",
                cache: false,
                data: {

               id:id
                },
                success: function (response) {

                    if(response){
                        $('#hid').val(response.id);
                        $('#name').val(response.name);
                        $('#des').val(response.description);



                    }else{
                        alert('Error')
                    }



                }, error: function (xhr) {
                    console.log('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                    alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);

                    // $('#title_error').text(xhr.responseJSON.errors.grn_title);
                    // $('#description_error').text(xhr.responseJSON.errors.grn_description );
                    // $('#quertity_error').text(xhr.responseJSON.errors.quentity );
                    // $('#supplier_error').text(xhr.responseJSON.errors.supplier);



                }
            })


    },
    delete: function (id) {



        var chk=confirm('Are you sure ?');


        if(chk){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN':
                        $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/admin/deleteArea",
                type: "POST",
                cache: false,
                data: {

           id:id
                },
                success: function (response) {
                    if(response=="success"){
                        console.log('data has been deleted');
                       window.location.reload();
                    }else{
                        console.log('Fail To delete');
                        window.location.reload();


                    }




                }, error: function (xhr) {
                    console.log('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                    alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);




                }
            })

        }
    },


}
