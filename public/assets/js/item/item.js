var item={
    save: function () {


        var cat_code=$('#cat_code').val();
        var item_code=$('#It_code').val();
        var name=$('#name').val();
        var description=$('#dis').val();
        var code=$('#code').val();
        var id=$('#hid').val();



        // alert('hi');


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN':
                        $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/saveItem",
                type: "POST",
                cache: false,
                data: {

                    cat_code:cat_code,
                    item_code:item_code,
                    name:name,
                    description:description,
                    code:code,
                    id:id



                },
                success: function (response) {
                    if(response=="Created"){
                        console.log('data has been saved');
                       window.location.reload();
                    }else{
                        console.log('data has been Updated');
                        window.location.reload();


                    }




                }, error: function (xhr) {
                    console.log('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                    alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);

                    // $('#title_error').text(xhr.responseJSON.errors.grn_title);
                    // $('#description_error').text(xhr.responseJSON.errors.grn_description );
                    // $('#quertity_error').text(xhr.responseJSON.errors.quentity );
                    // $('#supplier_error').text(xhr.responseJSON.errors.supplier);



                }
            })


    },

    edit: function (id) {



            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN':
                        $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: "/edit_id",
                type: "POST",
                cache: false,
                data: {

                id:id


                },
                success: function (response) {

                    if(response){
                    $('#hid').val(response.id)
                    $('#cat_code').val(response.category_code);
                    $('#It_code').val(response.item_code);
                    $('#name').val(response.name);
                    $('#dis').val(response.description);
                    $('#code').val(response.code);


                    }





                }, error: function (xhr) {
                    console.log('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                    alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);

                    // $('#title_error').text(xhr.responseJSON.errors.grn_title);
                    // $('#description_error').text(xhr.responseJSON.errors.grn_description );
                    // $('#quertity_error').text(xhr.responseJSON.errors.quentity );
                    // $('#supplier_error').text(xhr.responseJSON.errors.supplier);



                }
            })


    },

    delete: function (id) {

        var chk=confirm("Do you wish to delete ?");



    if(chk){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN':
                    $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            url: "/delete/"+id,
            type: "POST",
            cache: false,
            data: {

            id:id


            },
            success: function (response) {

                if(response=="success"){
                        alert('deleted')
                        location.reload(true);

                }else{
                    console.log("not deleted")
                }





            }, error: function (xhr) {
                console.log('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);
                alert('Request Status: ' + xhr.status + ' Status Text: ' + xhr.statusText + ' ' + xhr.responseText);

                // $('#title_error').text(xhr.responseJSON.errors.grn_title);
                // $('#description_error').text(xhr.responseJSON.errors.grn_description );
                // $('#quertity_error').text(xhr.responseJSON.errors.quentity );
                // $('#supplier_error').text(xhr.responseJSON.errors.supplier);



            }
        })
    }

},
}
