<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\LocationController;
use App\Http\Controllers\HotelController;
use App\Http\Controllers\GuestController;
use App\Http\Controllers\LoginController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/Login',[LoginController::class,'show']);

Route::post('/Auth/Login',[LoginController::class,'AuthUser']);

Route::get('/Signout',[LoginController::class,'Signout']);

Route::middleware(['authLogin'])->group(function () {


Route::prefix('admin/')->group(function()
{
    Route::get('location',[LocationController::class,'show']);
    Route::post('saveArea',[LocationController::class,'save']);
    Route::post('editArea',[LocationController::class,'editArea']);
    Route::post('deleteArea',[LocationController::class,'deletCategory']);

    Route::prefix('hotel/')->group(function()
    {
        Route::get('view',[HotelController::class,'show']);
        Route::post('hoteldetails',[HotelController::class,'save']);
        Route::post('edit',[HotelController::class,'edit']);
        Route::post('deleteHotel',[HotelController::class,'delete']);


    });

});

});


Route::prefix('guest/')->group(function()
{
    Route::get('view',[GuestController::class,'show']);
    Route::get('search',[GuestController::class,'search']);


    Route::prefix('view/')->group(function()
    {
    Route::get('location/{id}',[GuestController::class,'showlocation']);
    Route::post('inquiry',[GuestController::class,'sendMail']);

    });


});
