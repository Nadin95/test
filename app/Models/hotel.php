<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class hotel extends Model
{
    use HasFactory;


    protected $table='hotel';


    use SoftDeletes;
    protected $dates=['deleted_at'];



    protected $fillable=[
        'name','location_id','description'
    ];
}
