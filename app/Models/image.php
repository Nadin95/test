<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class image extends Model
{
    use HasFactory;


    protected $table='image';


    use SoftDeletes;
    protected $dates=['deleted_at'];
}
