<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\location;
use App\Models\hotel;
use App\Models\image;


class HotelController extends Controller
{
    public function show()
    {
        $location =location::where('deleted_at',null)->get();
        $hotel=hotel::where('deleted_at',null)->paginate(10);


        return view('hotel.hotel')->with([
            'location'=>$location,
            'hotel'=>$hotel,
            // 'page_heading'=>$page_heading
        ]);
    }

    public function save(Request $req)
    {

        $hotel_name=$req->name;
        $description=$req->description;
        $location_id=$req->location_id;
        $id=$req->hid;

        if(!empty($id)){
            $this->update_hotel($id,$hotel_name,$description,$location_id);
        }

            $this->savehoteldetail($hotel_name,$description,$location_id);
            $hotel_details=hotel::where('name',$hotel_name)->first();



        if($req->hasFile('files'))
        {
            $path= public_path().'/hotel-photo/'.$hotel_details->name.'/';


            if(!(image::exists($path)))
            {
                \File::makeDirectory($path,$mode=0775,true,true);
            }

            foreach ($req->file('files') as $file)
               {
                   $file_name=$file->getClientOriginalName();

                   $file->move($path,$file_name);
                   $url='hotel-photo/'.$hotel_details->name.'/'.$file_name;

                   $image = new image();
                   $image->location_id = $location_id;
                   $image->hotel_id = $hotel_details->id;
                   $image->url = $url;
                   $image->save();

               }

        }

        return redirect()->back();
    }

    public function savehoteldetail($hotel_name,$description,$location_id)
    {
        $saveHotel=hotel::create([
             'name'=>$hotel_name,
             'location_id'=>$location_id,
             'description'=>$description,
        ]);

    }

    public function update_hotel($id,$hotel_name,$description,$location_id)
    {
        $update=hotel::where('deleted_at',null)->where('id',$id)->update([
            'name'=>$hotel_name,
             'location_id'=>$location_id,
             'description'=>$description,

        ]);




    }


    public function edit(Request $req)
    {
        $id=$req->id;
        $hotel_details=hotel::where('id',$id)->where('deleted_at',null)->first();
        return response()->json($hotel_details);


    }

    public function delete(Request $req)
    {
        $id=$req->id;

        $delete_data=hotel::find($id);
        $delete_data->delete();

        return response()->json('deleted');

    }
}
