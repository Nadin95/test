<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;


class ItemController extends Controller
{
    //

    public function show()
    {
        $items=Item::where('deleted_at',null)->paginate(10);
        return view('items.items')->with([
            'items'=>$items
        ]);
    }

    public function SaveItem(Request $req)
    {

        $data=$req->validate([
            'cat_code'=>'required',
            'item_code'=>'required',
            'name'=>'required',
            'description'=>'required',
            'code'=>'required|integer',

        ]);

        switch($req->id){

        case($req->id):
            $this->update($req->id,$data['cat_code'],$data['item_code'],$data['name'],$data['description'],$data['code']);
            return response()->json('Updated');
            break;
        default:


            $items=Item::create([

                'category_code'=>$data['cat_code'],
                'item_code'=>$data['item_code'],
                'name'=>$data['name'],
                'description'=>$data['description'],
                'code'=>$data['code'],

            ]);
            return response()->json('Created');
        }



    }

    public function editid(Request $req)
    {
        $id=$req->id;

        $data=Item::where('id',$id)->where('deleted_at',null)->first();

        return response()->json($data);


    }
    public function update($id,$cat_code,$item_code,$name,$description,$code)
    {
        $update=Item::where('id',$id)->where('deleted_at',null)->update([

            'category_code'=>$cat_code,
            'item_code'=>$item_code,
            'name'=>$name,
            'description'=>$description,
            'code'=>$code,

        ]);




    }

   public function deleteItem($id)
   {
    //    $id=$req->req;
    //    Request $req


    $item=Item::where('id',$id)->firstOrFail();

    if ($item != null) {
    $item->delete();
    return response()->json('success');
    }else{
        return response()->json('error');
    }

   }
}
