<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\location;
use App\Models\hotel;
use App\Models\image;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendMail;
use Session;

class GuestController extends Controller
{
    //

    public function show()
    {
        $location=location::where('deleted_at',null)->get();
        $hotel=hotel::where('deleted_at',null)->get();
        $image=image::where('deleted_at',null)->get();


        return view('guest.guest')->with([
            'location'=>$location,
            'hotel'=>$hotel,
            'image'=>$image,
            // 'page_heading'=>$page_heading

        ]);

    }

    public function search(Request $request)
    {
        $search=$request->get('search');


        $location=location::where('name', 'like', '%'.$search.'%')
        // ->orWhere('customer_name', 'like', '%' . $search . '%')
        ->orderBy('name')
        ->paginate(2);

        return view('guest.guest')->with([

            'location'=>$location,

        ]);
    }

    public function showlocation($id)
    {
        $images=image::where('location_id',$id)->get();
        $page_heading =location::where('deleted_at',null)->where('id',$id)->first();

        return view('guest.locationView')->with([
            'images'=>$images,
            'page_heading'=>$page_heading

        ]);

    }

    public function sendMail(Request $request)
    {
        $this->validate($request,[

            'name'=>'required',
            'contact'=>'required',
            'mail'=>'required|email',
            'inquiry'=>'required',

        ]);

        $data = array(
            'name'=>$request->name,
            'contact'=>$request->contact,
            'mail'=>$request->mail,
            'inquiry'=>$request->inquiry

        );


        // Mail::to($request->user())->send(new SendMail($data['inquiry']));
        Mail::send('emails,contact',$data, function ($message) use ($data)
        {
            $message->from($data['mail']);
            $message->to('');
            $message->subject('Inquire from FindHotel');

        });

        Session::flash('success','Your email was sent');

        return Redirect::back();



    }
}
