<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\location;

class LocationController extends Controller
{
    public function show()
    {
        $location=location::where('deleted_at',null)->paginate(10);
        return view('area.area')->with([
            'location'=>$location
        ]);
    }

   public function save(Request $req)
   {

    $data=$req->validate([
        'name'=>'required',
        'description'=>'required',

    ]);


    if($req->id)
        {
                $this->update($req->id,$data['name'],$data['description']);
                return response()->json('updated');

            }else{

            $save_category=location::create([
                'name'=>$data['name'],
                'description'=>$data['description'],

            ]);
            return response()->json('created');

        }


   }

   public function update($id,$name,$description)
   {
       $update_category=location::where('id',$id)->update([
           'name'=>$name,
           'description'=>$description,

       ]);

   }

   public function editArea(Request $req)
   {
       $id=$req->id;

       $editData=location::where('id',$id)->where('deleted_at',null)->first();
       return response()->json($editData);

   }

   public function deleteArea(Request $req)
   {
       $id=$req->id;


       location::find($id)->delete();

    return response()->json('success');


   }




}
