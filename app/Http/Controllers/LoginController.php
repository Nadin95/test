<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\admin;
use Illuminate\Support\Facades\Hash;
use Session;


class LoginController extends Controller
{

    public function show()
    {
        return view('login.login');
    }

    public function AuthUser(Request $request){

        $email = $request->email;
        $pass = $request->password;

        // dd($pass);
        $data = admin::where('mail', $email)->first();
    //    dd($data->password);
        // dd(Hash::check($pass,$data->password));

        if(!empty($data) && Hash::check($pass,$data->password)){
            $request->session()->put('admin', $data);
            return redirect('/admin/location');
        }else{
            return redirect('/Login?error=true');
        }
    }

    public function Signout(){

        Session::flush();
        return redirect('/Login');

    }
}
