<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{

    public function showCategory()
    {
        $category=category::where('deleted_at',null)->get();

        return view('category.category')->with([
            'category'=>$category
        ]);
    }

    public function saveCategory(Request $req)
    {

        $data=$req->validate([
            'name'=>'required',
            'description'=>'required',
            'code'=>'required',

        ]);

        switch($req->id)
        {
            case($req->id):

                $this->update($req->id,$data['name'],$data['description'],$data['code']);
                return response()->json('updated');


            break;
            default:



            $save_category=category::create([
                'categoty_name'=>$data['name'],
                'description'=>$data['description'],
                'code'=>$data['code']
            ]);


            return response()->json('created');


        }



    }

    public function editCategory(Request $req)
    {
        $id=$req->id;

        $editData=category::where('id',$id)->where('deleted_at',null)->first();

        return response()->json($editData);

    }

    public function update($id,$name,$description,$code)
    {
        $update_category=category::where('id',$id)->update([
            'categoty_name'=>$name,
            'description'=>$description,
            'code'=>$code
        ]);

    }

    public function deletCategory(Request $req)
    {
        $id=$req->id;

        $delete_data=category::find($id);
        $delete_data->delete();

        return response()->json('deleted');


    }


}
