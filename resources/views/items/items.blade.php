@extends('layout.layout1')

@section('content')



    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <input type="text" id="cat_code" class="form-control" placeholder="Enter Category code" required>
                    </div>
                    <div class="col">
                        <input type="text" id="It_code" class="form-control" placeholder="Enter  Item Code" required>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="text" id="name" class="form-control" placeholder="Enter Name" required>
                    </div>

                    <div class="col">
                        <textarea type="text" id="dis" class="form-control" placeholder="Descrip" required></textarea>
                    </div>

                </div>

                <div class="row">
                    <div class="col">

                        <input type="text-area" id="code" class="form-control" placeholder="Code" required>
                    </div>

                    <div class="col">
                        <input type="button" class="btn btn-primary btn-lg btn-block" onclick="item.save()" value="Save">
                        <input type="hidden" id="hid" value="">
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container">

        <table class="table table-border">
            <tr>

                <th>No</th>
                <th>Categoty code</th>
                <th>Item Code</th>
                <th>Name</th>
                <th>Description</th>
                <th>Code</th>
                <th>Operation</th>
            </tr>


            @if (count($items) >= 1)
                @foreach ($items as $item)
                    </tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->category_code }}</td>
                    <td>{{ $item->item_code }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->description }}</td>
                    <td>{{ $item->code }}</td>
                    <td>
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-warning"
                                onclick="item.edit({{ $item->id }})">Edit</button>
                            <button type="button" class="btn btn-danger"
                                onclick="item.delete({{ $item->id }})">Delete</button>
                        </div>
                    </td>
                    <tr>
                @endforeach
            @else
                <div class="alert alert-danger">
                    <strong>Sorry!</strong> No Product Found.
                </div>
            @endif




        </table>

    </div>
@endsection
