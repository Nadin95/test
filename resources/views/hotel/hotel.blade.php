@extends('layout.layout1')

@section('content')



    <div class="container">
        <div class="card">

            <div class="card-body">
                <form method="POST" enctype="multipart/form-data" id="upload-file"
                    action="{{ url('/admin/hotel/hoteldetails') }}">
                    @csrf

                    <div class="row">
                        <div class="col">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Enter Name"
                                required>
                        </div>

                        <div class="col">
                            <textarea type="text" name="description" id="des" class="form-control"
                                placeholder="Description" required></textarea>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col">
                            <select class="form-control" name="location_id" id="location_id">

                                @if ($location->count())
                                    @foreach ($location as $loc)
                                        <option value="{{ $loc->id }}">
                                            {{ $loc->name }}</option>
                                    @endforeach
                                @endif

                            </select>
                        </div>
                        <input type="file" id="files" name="files[]" class="dropify" data-max-file-size-preview="3M"
                            multiple="multiple" />
                        <div class="col">

                            <input type="hidden" id="hid" name="hid" value="">
                            <button class="btn btn-primary">Save</button>
                        </div>

                    </div>
                </form>







            </div>
        </div>
    </div>

    <div class="container">

        <table class="table table-border">
            <tr>

                <th>No</th>
                <th>Categoty code</th>
                <th>Item Code</th>
                <th>Name</th>
                <th>Description</th>
                <th>Code</th>
                <th>Operation</th>
            </tr>


            @if (count($hotel) >= 1)
                @foreach ($hotel as $item)
                    </tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->item_code }}</td>

                    <td>{{ $item->description }}</td>

                    <td>
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-warning"
                                onclick="hotel.edit({{ $item->id }})">Edit</button>
                            <button type="button" class="btn btn-danger"
                                onclick="hotel.delete({{ $item->id }})">Delete</button>
                        </div>
                    </td>
                    <tr>
                @endforeach
            @else
                <div class="alert alert-danger">
                    <strong>Sorry!</strong> No Product Found.
                </div>
            @endif




        </table>

    </div>
@endsection
