@extends('layout.layout1')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <input type="text" id="name" class="form-control" placeholder="Enter Category Name" required>
                        <span class="text-danger" id="name_error"></span>
                    </div>
                    <div class="col">
                        <input type="text" id="des" class="form-control" placeholder="Enter Category description" required>
                        <span class="text-danger" id="dis_error"></span>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <input type="text" id="code" class="form-control" placeholder="Enter Category code" required>
                        <span class="text-danger" id="code_error"></span>
                    </div>

                    <div class="col">
                        <input type="button" value="Save" class="btn btn-primary btn-lg btn-block"
                            onclick="category.save()">
                        <input type="hidden" value="" id="c_hid">
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <table class="table">
            <thead>

                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">description</th>
                    <th scope="col">Number</th>
                </tr>
            </thead>


            @if (count($category) > 0)
                @foreach ($category as $cat)
                    <tr>
                        <td>{{ $cat->id }}</td>
                        <td>{{ $cat->categoty_name }}</td>
                        <td>{{ $cat->description }}</td>
                        <td>{{ $cat->code }}</td>
                        <td>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-warning"
                                    onclick="category.edit({{ $cat->id }})">Edit</button>
                                <button type="button" class="btn btn-danger"
                                    onclick="category.delete({{ $cat->id }})">Delete</button>
                            </div>
                        </td>


                    </tr>
                @endforeach
            @else
                <div class="alert alert-danger">
                    <strong>Sorry....!!!</strong>
                    <p>No Data Found</p>
                </div>
            @endif



        </table>
    </div>
@endsection
