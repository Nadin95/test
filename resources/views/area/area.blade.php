@extends('layout.layout1')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <input type="text" id="name" class="form-control" placeholder="Enter Location Name" required>
                        <span class="text-danger" id="name_error"></span>
                    </div>
                    <div class="col">
                        <input type="text" id="des" class="form-control" placeholder="Enter Location description" required>
                        <span class="text-danger" id="dis_error"></span>
                    </div>
                </div>
                <div class="row">


                    <div class="col">
                        <input type="button" value="Save" class="btn btn-primary btn-lg btn-block" onclick="area.save()">
                        <input type="hidden" value="" id="hid">
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <table class="table">
            <thead>

                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">description</th>
                    <th scope="col">Operation</th>
                </tr>
            </thead>



            @if (count($location) > 0)
                @foreach ($location as $loc)
                    <tr>
                        <td>{{ $loc->id }}</td>
                        <td>{{ $loc->name }}</td>
                        <td>{{ $loc->description }}</td>

                        <td>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-warning"
                                    onclick="area.edit({{ $loc->id }})">Edit</button>
                                <button type="button" class="btn btn-danger"
                                    onclick="area.delete({{ $loc->id }})">Delete</button>
                            </div>
                        </td>


                    </tr>
                @endforeach
            @else
                <div class="alert alert-danger">
                    <strong>Sorry....!!!</strong>
                    <p>No Data Found</p>
                </div>
            @endif



        </table>
    </div>
@endsection
