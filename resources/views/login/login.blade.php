<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/bootstrap/css/bootstrap.css') }}">

    <link type="text/css" rel="stylesheet" href="{{ URL::asset('assets/bootstrap/custom.css') }}">
</head>

<body>



    <div class="wrapper fadeInDown">
        <div id="formContent">
            <!-- Tabs Titles -->

            <!-- Icon -->
            <div class="fadeIn first">
                <img src="http://danielzawadzki.com/codepen/01/icon.svg" id="icon" alt="User Icon" />
            </div>

            <!-- Login Form -->
            <form method="POST" enctype="multipart/form-data" id="upload-file" action="{{ url('/Auth/Login') }}">
                @csrf


                <input type="text" id="login" class="fadeIn second" name="email" placeholder="login">
                <input type="text" id="password" class="fadeIn third" name="password" placeholder="password">
                {{-- <input type="password" id="password" class="fadeIn third" name="password" placeholder="password"> --}}
                <input type="submit" class="fadeIn fourth" value="Log In">
            </form>

            <!-- Remind Passowrd -->
            <div id="formFooter">
                <a class="underlineHover" href="#">Forgot Password?</a>
            </div>

        </div>
    </div>


</body>

</html>
