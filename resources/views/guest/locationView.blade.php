@extends('layout.layout2')




@section('content')
    <div class="container">
        {{-- @foreach ($images as $albumName) --}}

        {{-- name
        contact
        mail
        inquiry --}}
        {{-- <div class="card">
            <div class="card-body"> --}}
        <div class="container">
            <div>
                {{-- <i class="bi bi-backspace" href="{{ URL::asset('/guest/view') }}"> --}}
                {{-- <a class="nav-link bi bi-backspace" href="{{ URL::asset('/guest/view') }}"></a> --}}
                {{-- </i> --}}

                <a href="{{ URL::asset('/guest/view') }}"><i class="fa fa-backward fa-5x" aria-hidden="true"></i>
                </a>

            </div>
            <h5 class="card-title">Inquary</h5>

            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert"></button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form method="POST" enctype="multipart/form-data" id="upload-file" action="{{ url('/guest/view/inquiry') }}">
                @csrf
                <div class="row">

                    <div class="col">
                        <input type="text" name="name" id="name" class="form-control" placeholder="Enter Name" required>

                    </div>
                    <div class="col">
                        <input type="text" id="c_number" name="contact" class="form-control"
                            placeholder="Enter Contact number" required>
                    </div>
                </div>


                <div class="row">
                    <div class="col">
                        <input type="text" name="mail" id="mail" class="form-control" placeholder="Enter Mail" required>
                    </div>
                    <div class="col-sm">
                        <textarea id="" class="form-control" name="inquiry" placeholder="Inquiry" required></textarea>
                    </div>
                </div>

                <div class="row">
                    <div class="col">
                        <input type="submit" value="Submit" class="btn btn-info">
                    </div>
                </div>

            </form>


        </div>

        {{-- </div>
        </div> --}}
        <div class="card mb-4 border-0">

            <h1>
                {{ $page_heading->name }}
            </h1>



            <div class="card-body">


                <div class="d-flex image-album" id="style-1">

                    @foreach ($images as $image)
                        <div class="col-lg-3 album-img">

                            <a href="/{{ $image->url }}" download="{{ $image->name }}" target="_blank">
                                <div class="card">
                                    <img src="/{{ $image->url }}" alt="">
                                </div>
                            </a>

                        </div>
                    @endforeach

                </div>
            </div>

        </div>



    </div>
@endsection
