@extends('layout.layout2')

@section('content')
    <div class="container">
        <form action="/guest/search" method="get">
            <div class="input-group">
                <input type="search" class="form-control rounded" placeholder="Search Location" aria-label="Search"
                    aria-describedby="search-addon" />
                <button type="submit" class="btn btn-outline-primary">search</button>
            </div>
        </form>


        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Location</th>
                    <th scope="col">Description</th>

                    <th scope="col">Operation</th>


                </tr>
            </thead>
            <tbody>
                @foreach ($location as $loc)
                    <tr>
                        <td>{{ $loc->id }}</td>
                        <td>{{ $loc->name }}</td>
                        <td>{{ $loc->description }}</td>


                        <td>

                            <button type="button" class="btn btn-secondary"><a
                                    href="view/location/{{ $loc->id }}">view</button>

                            {{-- <button type="button" class="btn btn-secondary"
                                onclick="loc.edit({{ $loc->id }})">View</button> --}}


                        </td>
                @endforeach
                </tr>

            </tbody>
        </table>
    </div>
@endsection
